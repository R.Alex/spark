import org.apache.spark.api.java.JavaRDD;
import java.util.Arrays;
import org.apache.commons.lang.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.api.java.function.Function; -- no need when lambda used

public final class T03_SimpleMap {
    public static void run() throws Exception {

        // Create a Java Spark Context.
        SparkConf conf = new SparkConf().setAppName("SimpleMap").setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Create RDD out of static array of integers
        JavaRDD<Integer> rdd = sc.parallelize(Arrays.asList(1, 2, 3, 4));

        // Do mapping x -> x*x
        JavaRDD<Integer> result = rdd.map(x -> x*x
                //new Function<Integer, Integer>() { public Integer call(Integer x) { return x*x;}}
                );
        System.out.println(StringUtils.join(result.collect(), ","));

        sc.stop();
    }
}
