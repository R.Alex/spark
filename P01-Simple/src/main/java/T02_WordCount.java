import scala.Tuple2;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import java.util.Arrays;
//import java.util.Iterator; -- no need when lambda used
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.api.java.function.FlatMapFunction; -- no need when lambda used
//import org.apache.spark.api.java.function.Function2; -- no need when lambda used
//import org.apache.spark.api.java.function.PairFunction; -- no need when lambda used

public final class T02_WordCount { //01/31/2019

    public static void run() throws Exception
    {
        String inputFile = "C:\\Java\\Spark\\files\\words.txt";
        String outputFile = "C:\\Java\\Spark\\files\\words.out";

        // Create a Java Spark Context.
        SparkConf conf = new SparkConf().setAppName("wordCount").setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Load our input data.
        JavaRDD<String> input = sc.textFile(inputFile);

        // Split up into words.
        JavaRDD<String> words = input.flatMap( x -> Arrays.asList(x.split(" ")).iterator()
/*
                new FlatMapFunction<String, String>() {
                    public Iterator<String> call(String x) {
                        return Arrays.asList(x.split(" ")).iterator();
                    }}
*/
                    );

        // Transform into word and count.
        JavaPairRDD<String, Integer> counts = words.mapToPair( x -> new Tuple2(x, 1))
                  .reduceByKey((x, y) -> ((Integer)x+(Integer)y));
/*
                new PairFunction<String, String, Integer>(){
                    public Tuple2<String, Integer> call(String x){
                        return new Tuple2(x, 1);
                    }}).reduceByKey(new Function2<Integer, Integer, Integer>(){
            public Integer call(Integer x, Integer y){ return x + y;}}
*/


        // Save the word count back out to a text file, causing evaluation.
        //counts.saveAsTextFile(outputFile); //Commented as this fails
        for (Tuple2<?,?> tuple : output) {
            System.out.println(tuple._1() + ": " + tuple._2());
        }
        sc.stop();
    }
}

